# STALKER (WIP) :

License for code: MIT

License for Texture/Model : CC-by-SA 4.0


A terrifying and very strong monster that lives in caves, and can follow you anywhere...
This mob is a test, it's not perfect and it was inspired by the "Cave Dweller" mod, but it's really fun and makes the caves a little more sinister and dangerous!!!